<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function biodata()
    {
        return view('Halaman/biodata', [
            "title" => "Biodata"
        ]);
    }

    // public function selamat(Request $request)
    // {
    //     $nama = $request['nama'];
    //     $alamat = $request['alamat'];
    //     return view('Halaman/selamat_datang', [
    //         "title" => "Welcome",
    //         "nama" => $nama,
    //         "alamat" => $alamat
    //     ]);
    // }

    public function master(Request $request)
    {
        $nama = $request['nama'];
        $alamat = $request['alamat'];
        $lastname = $request['lastname'];
        return view('Halaman/selamat_datang', [
            "title" => "Dashboard",
            "nama" => $nama,
            "alamat" => $alamat,
            "lastname" => $lastname
        ]);
    }

    public function data_table()
    {
        return view('table/data-table', [
            "title" => "Data Tabel"
        ]);
    }

    public function table()
    {
        return view('table/table', [
            "title" => "Tabel"
        ]);
    }

    // public function create()
    // {
    //     return view('cast/create', [
    //         "title" => "Cast"
    //     ]);
    // }
}
