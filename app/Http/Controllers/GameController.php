<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GameController extends Controller
{
    public function create()
    {
        return view('games/create', [
            'title' => "Tambah Data Game"
        ]);
    }

    public function game(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'gameplay' => 'required',
            'developer' => 'required',
            'year' => 'required',
        ]);

        DB::table('game')->insert([
            'name' => $request['name'],
            'gameplay' => $request['gameplay'],
            'developer' => $request['developer'],
            'year' => $request['year']
        ]);

        return redirect('/games');
    }

    public function index()
    {
        $games = DB::table('game')->get();
        return view('games/index', compact('games'), [
            'title' =>
            "Halaman Utama Game"
        ]);
    }

    public function show($id)
    {
        $games = DB::table('game')->where('id', $id)->first();
        return view('games.show', compact('games'), [
            'title' => "Tampilan List Game"
        ]);
    }

    public function edit($id)
    {
        $games = DB::table('game')->where('id', $id)->first();
        return view('games.edit', compact('games'), [
            'title' => "Edit Nama Game"
        ]);
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'name' => 'required',
            'gameplay' => 'required',
            'developer' => 'required',
            'year' => 'required',
        ]);

        $query = DB::table('game')
            ->where('id', $id)
            ->update([
                'name' => $request["name"],
                'gameplay' => $request["gameplay"],
                'developer' => $request["developer"],
                'year' => $request["year"]
            ]);
        return redirect('/games');
    }

    public function destroy($id)
    {
        $query = DB::table('game')->where('id', $id)->delete();
        return redirect('/games');
    }
}
