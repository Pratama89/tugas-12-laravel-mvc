<!doctype html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">

<title>Show Data Game</title>
</head>
<body>

  <div class="container mt-5">
    <div class="card" style="width: 18rem;">
      <div class="card-body">
        <h5 class="card-title">No {{$games->id}}</h5>
        <h5 class="card-title">{{$games->name}}</h5>
        <h6 class="card-subtitle mb-2 text-muted">{{$games->gameplay}}</h6>
        <p class="card-text">{{$games->developer}}</p>
        <p class="card-text">{{$games->year}}</p>
        <a href="/games" class="card-link">Kembali</a>
      </div>
    </div>
</div>
      
<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>

  
</body>
</html>