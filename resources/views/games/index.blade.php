@extends('layouts/master')

@section('judul')
    Tabel Data
@endsection

@section('content')

<div class="container mt-5">
<h2>List Game</h2>
<a href="/games/create" class="btn btn-primary mb-2">Tambah</a>
{{-- //isi link a href menuju ke form tambah data --}}

<table class="table">
<thead class="thead-light">
<tr>
<th scope="col">#</th>
<th scope="col">Name</th>
<th scope="col">Gameplay</th>
<th scope="col">Developer</th>
<th scope="col">Year</th>
<th scope="col" >Actions</th>
</tr>
</thead>
<tbody>
  @forelse ($games as $key=>$value)
        <tr>
            <td>{{$key + 1}}</th>
            <td>{{$value->name}}</td>
            <td>{{$value->gameplay}}</td>
            <td>{{$value->developer}}</td>
            <td>{{$value->year}}</td>
            <td>
                <form action="/games/{{$value->id}}" method="POST">
                <a href="/games/{{$value->id}}" class="btn btn-sm  btn-info" >Show</a>
                <a href="/games/{{$value->id}}/edit" class="btn btn-sm  btn-primary" >Edit</a>
                    @csrf
                    @method('DELETE')
                    <input type="submit" class="btn btn-danger btn-sm my-1" value="Delete">
                </form>
            </td>
        </tr>
    @empty
        <tr colspan="3">
            <td>Data Masih Kosong</td>
        </tr>  
    @endforelse          

{{-- //code disini tampilakan semua data di database dan buat link dan tombol untuk edit, detail, dan delete --}}

</tbody>

</table>

</div>


@endsection