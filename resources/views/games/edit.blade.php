<!doctype html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">

<title>Edit Data</title>
</head>
<body>
  <div class="container mt-5">
  <h2>Edit Data Game</h2>
      <form action="/games/{{ $games->id }}" method="post">
              @csrf
              @method('PUT')
              <div class="form-group">
                <label >Nama</label>
                <input type="text" class="form-control" name="name" value="{{$games->name}}" placeholder="Tambah Nama">
              </div>
              @error('name')
                  <div class="alert alert-danger">{{ $message }}</div>
              @enderror
              <div class="form-group">
                <label >Game Play</label>
                <input type="text" class="form-control" name="gameplay" value="{{$games->gameplay}}" placeholder="Tambah Game Play">
              </div>
              @error('gameplay')
                  <div class="alert alert-danger">{{ $message }}</div>
              @enderror
              <div class="form-group">
                <label >Developer</label>
                <input type="text" class="form-control" name="developer" value="{{$games->developer}}" placeholder="Tambah Developer">
              </div>
              @error('developer')
                  <div class="alert alert-danger">{{ $message }}</div>
              @enderror
              <div class="form-group">
                <label for="exampleInputPassword1">Year </label>
                <input type="number" value="{{$games->year}}" name="year" class="form-control" id="year" placeholder="Tambah Tahun">
              </div>
              @error('year')
                  <div class="alert alert-danger">{{ $message }}</div>
              @enderror
              
              
              <button type="submit" class="btn btn-primary">Edit</button>
      </form>

</div>


<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>

</body>

</html>