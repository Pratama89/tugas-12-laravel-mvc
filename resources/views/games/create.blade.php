@extends('layouts/master')

@section('judul')
    Create Data
@endsection

@section('content')
  <div class="container mt-5">
  <h2>Create Data Game</h2>
      <form action="/games" method="post">
              @csrf
              <div class="form-group">
                <label >Nama</label>
                <input type="text" class="form-control" name="name" placeholder="Tambah Nama">
              </div>
              @error('name')
                  <div class="alert alert-danger">{{ $message }}</div>
              @enderror
              <div class="form-group">
                <label >Game Play</label>
                <input type="text" class="form-control" name="gameplay" placeholder="Tambah Game Play">
              </div>
              @error('gameplay')
                  <div class="alert alert-danger">{{ $message }}</div>
              @enderror
              <div class="form-group">
                <label >Developer</label>
                <input type="text" class="form-control" name="developer" placeholder="Tambah Developer">
              </div>
              @error('developer')
                  <div class="alert alert-danger">{{ $message }}</div>
              @enderror
              <div class="form-group">
                <label for="exampleInputPassword1">Year </label>
                <input type="number" name="year" class="form-control" id="year" placeholder="Tambah Tahun">
              </div>
              @error('year')
                  <div class="alert alert-danger">{{ $message }}</div>
              @enderror
              
              
              <button type="submit" class="btn btn-primary">Simpan</button>
          </form>
    </div>
@endsection