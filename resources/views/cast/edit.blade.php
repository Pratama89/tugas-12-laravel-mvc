@extends('layouts/master')

@section('judul')
    Halaman Edit Data {{ $cast->nama }}
@endsection
@section('content')

<div>
        <h2>Edit Cast {{$cast->id}}</h2>
        <form action="/cast/{{$cast->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" id="nama" placeholder="Masukkan Nama">
                @error('nama')
                    <div class="alert alert-success mt-2">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="umur">Umur</label>
                <input type="number" class="form-control" name="umur"  value="{{$cast->umur}}"  id="body" placeholder="Masukkan Umur">
                @error('umur')
                    <div class="alert alert-success mt-2">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="bio">Bio</label>
                <textarea name="bio" id="bio" cols="30" rows="10" class="form-control" value="{{$cast->umur}}" placeholder="Masukkan Bio"></textarea>
                @error('bio')
                    <div class="alert alert-success mt-2">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary" >  Edit  </button>
            {{-- <button class="btn btn-primary mb-2"><a href="/cast"></a>Kembali</button> --}}
        </form>
    </div>

    @endsection