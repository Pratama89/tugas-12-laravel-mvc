@extends('layouts/master')

@section('judul')
    Halaman Detail Cast {{ $cast->nama }}
@endsection
@section('content')



<div class="card border-light mb-3" style="max-width: 18rem;">
  <div class="card-header">{{$cast->nama}}</div>
  <div class="card-body">
    <h5 class="card-title">{{$cast->umur}}</h5>
    <p class="card-text">{{$cast->bio}}</p>
    <a href="/cast" class="card-link">Kembali</a>
  </div>
</div>


@endsection