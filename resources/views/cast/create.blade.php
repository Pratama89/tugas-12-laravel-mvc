@extends('layouts/master')

@section('judul')
Cast
@endsection
@section('content')

<form action="/cast" method="post">
  @csrf
  <div class="form-group">
    <label>Judul Cast</label>
    <input type="text" class="form-control" name="nama" placeholder="Tambah Judul">
  </div>
  @error('nama')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label for="exampleInputPassword1">Umur</label>
    <input type="number" name="umur" class="form-control" id="umur" placeholder="Tambah Umur">
  </div>
  @error('umur')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Bio</label>
    <textarea class="form-control" name="bio" id="bio" cols="30" rows="10" placeholder="Tambah Bio"></textarea>
  </div>
  @error('bio')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <button type="submit" class="btn btn-primary">Simpan</button>
</form>

@endsection