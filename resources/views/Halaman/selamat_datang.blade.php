@extends('layouts/master')

@section('judul')
    Dashboard
@endsection
@section('content')

    <h1>Selamat Datang {{ $nama }} {{ $lastname }} di Website Kami!!</h1>
    <p>Nama: {{ $nama }}</p>
    <p>Alamat: {{ $alamat }}</p>
    <h3>Terima kasih telah bergabung di Website Kami. Media Belajar kita bersama!</h3>


@endsection