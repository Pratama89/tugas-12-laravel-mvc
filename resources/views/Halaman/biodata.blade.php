@extends('layouts/master')

@section('judul')
Biodata Baru
@endsection

@section('content')
    
    <h1>Buat account baru!</h1>
    <h2>Sign Up form</h2>
    <form action="/master" method="post">
        @csrf
        <label for="">First Name:</label> <br> <br>
        <input type="text" name="nama"> <br> <br>
        <label for="">Last Name:</label> <br> <br>
        <input type="text" name="lastname"> <br> <br>
        <label for="">Jenis Kelamin :</label> <br> <br>
        <input type="radio" name="lk" value="laki-laki"> Laki-Laki <br>
        <input type="radio" name="pr" value="perempuan"> Perempuan <br>
        <input type="radio" name="ln" value="lainnya"> Lainnya <br><br>
        <label for="">Warga Negara</label> <br> <br>
        <select name="wn" id="">
            <option value="1">Indonesia</option>
            <option value="2">Malaysia</option>
            <option value="3">Philipina</option>
            <option value="4">Singapure</option>
            <option value="4">Japan</option>
        </select> <br> <br>
        <label for="">Language Spoken:</label> <br> <br>
        <input type="checkbox" name="skill"> Bahasa Indonesia <br>
        <input type="checkbox" name="skill"> English <br>
        <input type="checkbox" name="skill"> Other <br> <br>
        <label for="">Bio:</label> <br> <br>
        <textarea name="alamat" id="" cols="30" rows="10"></textarea> <br><br>

        <input type="submit" value="Kirim">

       </form>

@endsection