@push('style')
    <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
@endpush

<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="../../index3.html" class="brand-link">
      <img src="{{ asset('admin/dist/img/logo pratama.png') }}" alt="AdminLTE Logo" class="brand-image rounded-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Blog Pratama</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset('admin/dist/img/Pratama 16x16px.jpg') }}" class="rounded-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Pratama</a>
        </div>
      </div>

      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="/data_table" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                <i class="right"></i>
              </p>
            </a>            
          </li>

          <li class="nav-item">
            <a href="/table" class="nav-link">
              <i class="nav-icon fas fa-table"></i>
              <p>
                Tables                
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="/games" class="nav-link">
              <i class="nav-icon fas fa-basketball-ball"></i>
              <p>
                Game                
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="/cast" class="nav-link">
              <i class="nav-icon far fa-address-book"></i>
              <p>
                Cast                
              </p>
            </a>
          </li>
          
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>